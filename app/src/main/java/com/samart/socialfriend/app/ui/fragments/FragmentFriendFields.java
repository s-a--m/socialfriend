package com.samart.socialfriend.app.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.samart.socialfriend.app.R;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardListView;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class FragmentFriendFields extends Fragment {
    private CardListView cardList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_fields, container, false);
        this.cardList = (CardListView) view.findViewById(R.id.card_list_view_friend_fields);
        processView(cardList, getActivity());
        return view;
    }

    private static void processView(CardListView cardListView, Context context) {
        String[] strings = {"one", "two", "three", "four"};
        List<Card> cardsList = new ArrayList<Card>();
        for (String s : strings) {
            Card card = new Card(context);
            CardHeader header = new CardHeader(context);
            header.setTitle(s);
            card.addCardHeader(header);
            card.setTitle(s);
            cardsList.add(card);
        }
        cardListView.setAdapter(new CardArrayAdapter(context, cardsList));
    }
}
