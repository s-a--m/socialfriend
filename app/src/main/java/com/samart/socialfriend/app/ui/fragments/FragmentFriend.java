package com.samart.socialfriend.app.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.samart.socialfriend.app.MyPagerAdapter;
import com.samart.socialfriend.app.MyTabListener;
import com.samart.socialfriend.app.R;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class FragmentFriend extends Fragment {
    private ViewPager viewPager;
    private MyPagerAdapter pagerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_friend, null);
        this.viewPager = (ViewPager) view.findViewById(R.id.pager);
        manageTabsAndPager((ActionBarActivity) getActivity());
        return view;
    }

    @Override
    public void onDetach() {
        ActionBarActivity a = (ActionBarActivity) getActivity();
        a.getSupportActionBar().removeAllTabs();
        super.onDetach();
    }

    private void manageTabsAndPager(ActionBarActivity activity) {
        ActionBar ab = activity.getSupportActionBar();
        ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);


        String[] tabsTitles = {"Info", "Notes", "Events"};
        setUpPager(tabsTitles, activity);
        MyTabListener listener = new MyTabListener(viewPager);

        for (String tabsTitle : tabsTitles) {
            ab.addTab(ab.newTab().setText(tabsTitle).setTabListener(listener));
        }
    }

    private void setUpPager(String[] tabsTitles, ActionBarActivity activity) {
        if (null == this.pagerAdapter) pagerAdapter =
                new MyPagerAdapter(activity, getChildFragmentManager(), tabsTitles);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOnPageChangeListener(pagerAdapter);
    }

}
