package com.samart.socialfriend.app.db.tables;

import android.provider.BaseColumns;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public abstract class TableColumnsFriends implements BaseColumns {

    public static final String TABLE_NAME = "friends";
    public static final String COLUMN_NAME_FNAME = "fname";
    public static final String COLUMN_NAME_SNAME = "sname";
    public static final String COLUMN_NAME_LNAME = "lname";
    public static final String[] PROJECTION = {COLUMN_NAME_FNAME, COLUMN_NAME_SNAME, COLUMN_NAME_LNAME};
    public static final String SORT_BY_FNAME = COLUMN_NAME_FNAME+" DESC";
    public static final String SQL_CREATE_TABLE = "create table " +
            TABLE_NAME +
            " (" +
            _ID +
            " integer primary key autoincrement, " +
            COLUMN_NAME_FNAME +
            " text," +
            COLUMN_NAME_SNAME +
            " text," +
            COLUMN_NAME_LNAME +
            " text)";
    public static final String COLUMN_NAME_NULLABLE = "NULL";
}
