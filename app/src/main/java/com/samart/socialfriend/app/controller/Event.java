package com.samart.socialfriend.app.controller;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public abstract class Event implements Runnable, Comparable<Event> {
    private int value;
    private long time = System.nanoTime();

    public Event(int value) {
        this.value = value;
    }

    public Event() {
        this(-1);
    }

    @Override
    public abstract void run();

    @Override
    public int compareTo(Event another) {
        if (this.value < 0 && another.value > 0) return -1;
        if (another.value < 0 && this.value > 0) return 1;
        if (this.value == another.value) {
            if (this.time > another.time) return 1;
            else if (this.time < another.time) return -1;
            else return 0;
        } else if (this.value > another.value) {
            return 1;
        } else {
            return -1;
        }
    }
}
