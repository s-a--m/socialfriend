package com.samart.socialfriend.app;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;

import com.samart.socialfriend.app.ui.fragments.FragmentFriendDetails;
import com.samart.socialfriend.app.ui.fragments.FragmentFriendEvents;
import com.samart.socialfriend.app.ui.fragments.FragmentFriendNotes;

/**
* Copyright (C) 2014 Dmitry Samoylenko
* <p/>
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* <p/>
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* <p/>
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* <p/>
* Contact email dmitrysamoylenko@gmail.com
*/
public class MyPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

    private static final String[] FRAGMENT_NAMES = {FragmentFriendDetails.TAG, FragmentFriendNotes.TAG, FragmentFriendEvents.TAG};
    private final Fragment[] fragments = new Fragment[FRAGMENT_NAMES.length];
    private final ActionBarActivity mActivity;
    private final FragmentManager fragmentManager;
    private final String[] titles;

    public MyPagerAdapter(ActionBarActivity mainActivity,FragmentManager fm, String[] tabsTitles) {
        super(fm);
        this.mActivity = mainActivity;
        this.fragmentManager = fm;
        this.titles = tabsTitles;
    }

    @Override
    public Fragment getItem(int position) {
        return getFragment(position);
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    public Fragment getFragment(int position) {
        if (null == fragments[position]) {
            fragments[position] = fragmentManager.findFragmentByTag(FRAGMENT_NAMES[position]);
        }
        if (null == fragments[position]) {
            fragments[position] = Fragment.instantiate(mActivity, FRAGMENT_NAMES[position]);
        }
        return fragments[position];
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        mActivity.getSupportActionBar().setSelectedNavigationItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
