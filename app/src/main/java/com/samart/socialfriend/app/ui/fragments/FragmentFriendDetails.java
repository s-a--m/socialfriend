package com.samart.socialfriend.app.ui.fragments;

import android.content.Context;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.samart.socialfriend.app.R;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.internal.CardThumbnail;
import it.gmariotti.cardslib.library.view.CardView;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class FragmentFriendDetails extends Fragment {
    public static final String TAG = FragmentFriendDetails.class.getName();
    private CardView cardView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_details, null, false);
        this.cardView = (CardView) view.findViewById(R.id.card_view_friend_details);
        processViews(cardView, getActivity());
        return view;
    }
    private static void processViews(CardView cardView, Context context) {
        Card card = new Card(context,R.layout.card_inner_layout_my);
        CardThumbnail ct = new CardThumbnail(context);
        ct.setDrawableResource(R.drawable.ic_launcher);
        CardHeader header = new CardHeader(context);
        header.setTitle("Jon Skith");
        card.addCardHeader(header);
        card.addCardThumbnail(ct);
        card.setTitle("programmer");

        cardView.setCard(card);

    }

}
