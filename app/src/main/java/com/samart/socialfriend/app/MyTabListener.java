package com.samart.socialfriend.app;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

/**
* Copyright (C) 2014 Dmitry Samoylenko
* <p/>
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* <p/>
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* <p/>
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* <p/>
* Contact email dmitrysamoylenko@gmail.com
*/
public class MyTabListener implements android.support.v7.app.ActionBar.TabListener {

    private final ViewPager mViewPager;

    public MyTabListener(ViewPager viewPager) {
        this.mViewPager = viewPager;
    }

    @Override
    public void onTabSelected(android.support.v7.app.ActionBar.Tab tab,
                              FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

}
