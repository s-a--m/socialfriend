package com.samart.socialfriend.app.core;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class Friend implements Serializable, Comparable<Friend> {
    private static final long serialVersionUID = -1298512095782198l;
    public static final Comparator<Friend> COMPARE = new Comparator<Friend>() {
        @Override
        public int compare(Friend lhs, Friend rhs) {
            if (null == lhs.firstName || null == rhs.firstName) return -1;
            return lhs.firstName.compareTo(rhs.firstName);
        }
    };
    public String firstName;
    public String secondName;
    public String lastName;

    @Override
    public int compareTo(Friend another) {
        if (null == firstName) return -1;
        return firstName.compareTo(another.firstName);
    }
}
