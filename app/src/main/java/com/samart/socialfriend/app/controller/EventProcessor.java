package com.samart.socialfriend.app.controller;

import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class EventProcessor {
    private static final int POOL_SIZE = 1;
    private static final long KEEP_TIME = 5;
    private static final int QUEUE_INIT_CAP = 10;

    private final ThreadPoolExecutor pool = new ThreadPoolExecutor(POOL_SIZE,
            POOL_SIZE, KEEP_TIME,
            TimeUnit.SECONDS,
            new PriorityBlockingQueue<Runnable>(QUEUE_INIT_CAP, new Comparator<Runnable>() {
                @Override
                public int compare(Runnable lhs, Runnable rhs) {
                    Event e1 = (Event) lhs;
                    Event e2 = (Event) rhs;
                    return e1.compareTo(e2);
                }
            })
    );

    public EventProcessor() {

    }
}
