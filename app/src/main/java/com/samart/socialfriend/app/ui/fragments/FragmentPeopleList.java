package com.samart.socialfriend.app.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.samart.socialfriend.app.R;
import com.samart.socialfriend.app.core.Friend;
import com.woozzu.android.widget.IndexableListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class FragmentPeopleList extends Fragment {
    private ListView listViewPeople;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_friends_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean b = super.onOptionsItemSelected(item);
        if (item.getItemId() == R.id.menu_add_friend) {
            startFragmentAddFriend();
        }
        return b;
    }

    private void startFragmentAddFriend() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment newFragment = new FragmentFriendEdit();
        ft.replace(R.id.fragment, newFragment);
        ft.addToBackStack("");
        ft.commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.indexed_listview, container, false);
        this.listViewPeople = (IndexableListView) view.findViewById(
                R.id.list_view_people
        );
        manageListView(getActivity(), listViewPeople);
        return view;
    }

    private void manageListView(Context context, ListView listView) {
        Friend[] listFriends = getListFriends();
        listView.setAdapter(new MyBaseAdapter(context, listFriends));
        listView.setFastScrollEnabled(true);
    }

    private Friend[] getListFriends() {
        Friend[] result = new Friend[36];
        char c = 'A';
        for (int i = 0; i < 26; i++) {
            result[i] = new Friend();
            result[i].firstName = "" + c;
            c++;
        }
        for (int i = 26; i < 36; i++) {
            result[i] = new Friend();
            result[i].firstName = "F";
        }
        return result;
    }

    private static class MyBaseAdapter extends BaseAdapter implements SectionIndexer {
        private final Friend[] listFriends;
        private final LayoutInflater inflater;
        private final String[] sections;
        private final int size;
        private ArrayList<Integer> sectionForPosition;
        private ArrayList<Integer> positionForSection;

        public MyBaseAdapter(Context context, Friend[] listFriends) {
            this.size = listFriends.length;
            this.listFriends = listFriends;
            Arrays.sort(listFriends, Friend.COMPARE);
            this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.sections = createSections(listFriends);
        }


        private String[] createSections(Friend[] listFriends) {
            this.sectionForPosition = new ArrayList<Integer>();
            this.positionForSection = new ArrayList<Integer>();
            char last = listFriends[0].firstName.charAt(0);
            List<String> chars = new ArrayList<String>();
            chars.add("" + last);
            int pos = 0;
            sectionForPosition.add(pos);
            positionForSection.add(pos);
            for (int i = 1; i < size; i++) {
                char c = listFriends[i].firstName.charAt(0);
                if (c != last) {
                    chars.add("" + c);
                    last = c;
                    pos++;
                    positionForSection.add(i);
                }
                sectionForPosition.add(pos);
            }
            String[] result = new String[0];
            return chars.toArray(result);
        }

        @Override
        public int getCount() {
            return size;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object[] getSections() {
            return sections;
        }

        @Override
        public int getPositionForSection(int sectionIndex) {
            return positionForSection.get(sectionIndex);
        }

        @Override
        public int getSectionForPosition(int position) {
            return sectionForPosition.get(position);
        }

        class Holder {
            TextView textViewFName;
            TextView textViewSName;
            TextView textViewLName;
            ImageView imageView;
            int pos = -1;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (null == convertView) {
                convertView = inflater.inflate(R.layout.friends_list_item, null);
                holder = new Holder();
                holder.imageView = (ImageView) convertView.findViewById(R.id.image_view_friend_photo);
                holder.textViewFName = (TextView) convertView.findViewById(R.id.text_view_first_name);
                holder.textViewSName = (TextView) convertView.findViewById(R.id.text_view_second_name);
                holder.textViewLName = (TextView) convertView.findViewById(R.id.text_view_last_name);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }
            if (holder.pos != position) {
                bindView(holder, position);
            }

            return convertView;
        }

        private void bindView(Holder holder, int position) {
            Friend friend = listFriends[position];
            holder.textViewFName.setText(friend.firstName);
            //TODO
        }
    }
}
