package com.samart.socialfriend.app.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.samart.socialfriend.app.core.Friend;
import com.samart.socialfriend.app.db.tables.TableColumnsFriends;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "social_friend_db";
    public static volatile DatabaseHelper instance;

    public static void createDatabaseHelper(Context context) {
        if (null != instance) return;
        synchronized (DatabaseHelper.class) {
            if (null != instance) {
                instance = new DatabaseHelper(context.getApplicationContext());
            }
        }
    }

    public void addFriend(Friend friend) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableColumnsFriends.COLUMN_NAME_FNAME, friend.firstName);
        contentValues.put(TableColumnsFriends.COLUMN_NAME_SNAME, friend.secondName);
        contentValues.put(TableColumnsFriends.COLUMN_NAME_LNAME, friend.lastName);
        SQLiteDatabase database = getWritableDatabase();
        database.insert(TableColumnsFriends.TABLE_NAME,
                TableColumnsFriends.COLUMN_NAME_NULLABLE, contentValues);
    }

    public List<Friend> getFriends() {
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.query(TableColumnsFriends.TABLE_NAME,
                TableColumnsFriends.PROJECTION,
                null,
                null,
                null,
                null,
                TableColumnsFriends.SORT_BY_FNAME);
        cursor.moveToFirst();
        int indFName = cursor.getColumnIndexOrThrow(TableColumnsFriends.COLUMN_NAME_FNAME);
        int indSName = cursor.getColumnIndexOrThrow(TableColumnsFriends.COLUMN_NAME_SNAME);
        int indLName = cursor.getColumnIndexOrThrow(TableColumnsFriends.COLUMN_NAME_LNAME);
        List<Friend> result = new ArrayList<Friend>();
        do {
            Friend friend = new Friend();
            friend.firstName = cursor.getString(indFName);
            friend.secondName = cursor.getString(indSName);
            friend.lastName = cursor.getString(indLName);
            result.add(friend);
        } while (cursor.moveToNext());
        cursor.close();
        return result;
    }


    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TableColumnsFriends.SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
