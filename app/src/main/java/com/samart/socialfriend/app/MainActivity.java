package com.samart.socialfriend.app;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.samart.socialfriend.app.ui.fragments.FragmentFriend;
import com.samart.socialfriend.app.ui.fragments.FragmentPeopleCircles;
import com.samart.socialfriend.app.ui.fragments.FragmentPeopleEvents;
import com.samart.socialfriend.app.ui.fragments.FragmentPeopleGraph;
import com.samart.socialfriend.app.ui.fragments.FragmentPeopleList;


public class MainActivity extends ActionBarActivity {

    private ActionBarDrawerToggle navigationDrawerListener;
    private DrawerLayout navigationDrawer;
    private NavigationManager navigationManager;
    private ListView actionsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manageNavigationDrawer();
        manageActionBar();
        manageNavigation(0);

        readState(savedInstanceState);
    }

    interface StateSaver {
        void readState(Bundle savedState, MainActivity activity);

        void writeState(Bundle savedState, MainActivity activity);
    }

    enum SavedStateItems implements StateSaver {
        LEFT_PANEL_POSITION {
            @Override
            public void readState(Bundle savedState, MainActivity activity) {
                if (savedState.containsKey(name()))
                    activity.manageNavigation(savedState.getInt(name()));
            }

            @Override
            public void writeState(Bundle savedState, MainActivity activity) {
                savedState.putInt(name(), activity.navigationManager.getPosition());
            }
        };

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        writeState(outState);
    }

    private void writeState(Bundle bundle) {
        if (null == bundle) return;
        for (SavedStateItems item : SavedStateItems.values())
            item.writeState(bundle, this);
    }

    private void readState(Bundle bundle) {
        if (null == bundle) return;
        for (SavedStateItems item : SavedStateItems.values())
            item.readState(bundle, this);
    }

    private void manageNavigation(int position) {
        if (null == actionsList) {
            actionsList = (ListView) findViewById(R.id.actions_list);
            this.navigationManager = new NavigationManager(this, actionsList);
        }
        this.navigationManager.setPosition(position);
    }

    private void manageNavigationDrawer() {
        if (null == this.navigationDrawer) {
            navigationDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer);
            navigationDrawer.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
        }
        if (null == this.navigationDrawerListener) {
            navigationDrawerListener = new ActionBarDrawerToggle(this,
                    navigationDrawer,
                    R.drawable.ic_drawer,
                    R.string.ab_open_navigation, R.string.ab_close_navigation
            );
            navigationDrawerListener.setDrawerIndicatorEnabled(true);
        }
        navigationDrawer.setDrawerListener(navigationDrawerListener);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        if (null != navigationDrawerListener) navigationDrawerListener.syncState();
        readState(savedInstanceState);
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (null != navigationDrawerListener)
            navigationDrawerListener.onConfigurationChanged(newConfig);
        super.onConfigurationChanged(newConfig);
    }


    private void manageActionBar() {
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeButtonEnabled(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if (null != navigationDrawerListener) {
            navigationDrawerListener.onOptionsItemSelected(item);
        }

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static class NavigationManager extends BaseAdapter implements AdapterView.OnItemClickListener {
        private final LayoutInflater inflater;
        private final CategorieLauncherContext categorieLauncherContext;

        final String[] actions = {"Friends", "Best Friend", "Events", "Circles", "Notes"};
        final int[] icons = {R.drawable.friends, R.drawable.best_friend, R.drawable.events, R.drawable.circles, R.drawable.notes};

        public NavigationManager(MainActivity mainActivity, ListView actionsList) {
            this.inflater = mainActivity.getLayoutInflater();
            actionsList.setAdapter(this);
            actionsList.setOnItemClickListener(this);
            this.categorieLauncherContext = new CategorieLauncherContext(mainActivity.getSupportActionBar(),
                    mainActivity.getSupportFragmentManager(), actionsList);
        }

        @Override
        public int getCount() {
            return actions.length;
        }

        @Override
        public Object getItem(int position) {
            return actions[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Categories.categories[position].onClick(categorieLauncherContext);
        }

        public void setPosition(int position) {
            categorieLauncherContext.setPosition(position);
        }

        public int getPosition() {
            return categorieLauncherContext.position;
        }

        static class Holder {
            TextView textView;
            ImageView imageView;
            int pos;
        }

        private interface CategorieLauncher {
            View getView(View convertView, LayoutInflater inflater, String[] actionsTitles, int[] icons);

            void onClick(CategorieLauncherContext context);

            Fragment getFragment(FragmentManager fragmentManager);

            void manageActionBar(ActionBar actionBar);

            void bindData(Holder holder, String[] actionsTitles, int[] icons);
        }

        enum Categories implements CategorieLauncher {
            ALL_FRIENDS_CARDS {
                @Override
                public Fragment getFragment(FragmentManager fragmentManager) {
                    return new FragmentPeopleList();
                }
            },
            BEST_FRIEND_CARD {
                @Override
                public Fragment getFragment(FragmentManager fragmentManager) {
                    return new FragmentFriend();
                }
            },
            ALL_FRIENDS_EVENTS {
                @Override
                public Fragment getFragment(FragmentManager fragmentManager) {
                    return new FragmentPeopleEvents();
                }
            },
            ALL_FRIENDS_CIRCLES {
                @Override
                public Fragment getFragment(FragmentManager fragmentManager) {
                    return new FragmentPeopleCircles();
                }
            },
            ALL_FRIENDS_GRAPH {
                @Override
                public Fragment getFragment(FragmentManager fragmentManager) {
                    return new FragmentPeopleGraph();
                }
            };

            @Override
            public void manageActionBar(ActionBar actionBar) {
                actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            }

            @Override
            public void onClick(CategorieLauncherContext context) {
                int position = ordinal();
                if (context.isNewPosition(position)) {
                    manageActionBar(context.actionBar);
                    Fragment fragment = context.fragmentManager.findFragmentByTag(name());
                    if (null == fragment) fragment = getFragment(context.fragmentManager);
                    context.replaceFragment(fragment, position, name());
                }
            }

            @Override
            public View getView(View convertView, LayoutInflater inflater, String[] actionsTitles, int[] icons) {
                Holder holder;
                View view = convertView;
                if (null == view) {
                    view = inflater.inflate(R.layout.list_item_navigation_drawer, null);
                    holder = new Holder();
                    holder.textView = (TextView) view.findViewById(R.id.categorie_text);
                    holder.imageView = (ImageView) view.findViewById(R.id.categorie_image);
                    holder.pos = -1;
                    view.setTag(holder);
                } else {
                    holder = (Holder) view.getTag();
                }
                if (holder.pos != this.ordinal()) {
                    bindData(holder, actionsTitles, icons);
                }
                return view;
            }

            public void bindData(Holder holder, String[] actionsTitles, int[] icons) {
                holder.textView.setText(actionsTitles[ordinal()]);
                holder.imageView.setImageResource(icons[ordinal()]);
            }

            static Categories[] categories = Categories.values();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return Categories.categories[position].getView(convertView, inflater, actions, icons);
        }

        private static class CategorieLauncherContext {
            public final ActionBar actionBar;
            public final FragmentManager fragmentManager;
            private final ListView actionsList;
            public int position = -1;
            Categories categorie = Categories.ALL_FRIENDS_CARDS;

            public CategorieLauncherContext(ActionBar supportActionBar, FragmentManager supportFragmentManager, ListView actionsList) {
                this.actionBar = supportActionBar;
                this.fragmentManager = supportFragmentManager;
                this.actionsList = actionsList;
            }

            public boolean isNewPosition(int newPosition) {
                return position != newPosition;
            }

            public void replaceFragment(Fragment fragment, int newPosition, String name) {
                fragmentManager.beginTransaction().replace(R.id.fragment, fragment, name).commit();
                position = newPosition;
            }

            public void setPosition(int newPosition) {
                if (isNewPosition(newPosition)) {
                    actionsList.setItemChecked(position, false);
                    actionsList.setItemChecked(newPosition, true);
                    categorie = Categories.categories[newPosition];
                    categorie.onClick(this);
                    position = newPosition;
                }
            }
        }
    }
}
